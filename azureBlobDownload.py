import uuid
import json
# Instantiate a new BlobServiceClient using a connection string - set chunk size to 1MB
from azure.storage.blob import BlobServiceClient, BlobBlock

# Download the blob to a local file
# Add 'DOWNLOAD' before the .txt extension so you can see both files in the data directory
destination_file = "test.csv"
source_file = "yardi-occupancy/yardiOccupancy.csv"
local_file_name = "test"

azure_config = json.load(open("creds_azure.json"))
access_key = azure_config["access_key"]
connection_string = access_key


def download_blob():
    blob_service_client = BlobServiceClient.from_connection_string(connection_string)

    # Instantiate a new ContainerClient
    container_client = blob_service_client.get_container_client("dw-staging")

    try:
        # Instantiate a new source blob client
        blob_client = container_client.get_blob_client(source_file)
        # [START download_a_blob]
        with open(destination_file, "wb") as my_blob:
            download_stream = blob_client.download_blob()
            my_blob.write(download_stream.readall())
        # [END download_a_blob]
    except Exception as e:
        print(e)


download_blob()