import os
import re
import numpy as np
import pandas as pd
from datetime import datetime

output_dir = 'output'
output_files = [f for f in os.listdir(output_dir) if 'daily' in f and 'concat' not in f]

dates = sorted(list(set([re.findall("\d{8}", f)[0] for f in output_files])))

col_order = [
    'Installation', 'Neighborhood', 'as_of',
    
    'Total Units',
    'Vacant Offline Homes',
    'Available Units by Week',
    'Availability Percent',
    
    'Vacant Unassigned Homes', # 9
    'Notice Unassigned Homes',
    'Net Total Homes Available',
    'Total Availability',
    'Actual Occupied Homes',
    'Actual Occupied Homes Percent',
    'Variance Between Availability and Actual', #15
    'Vacant Preleased',
    'Occupied Notice Preleased',
    'Leased Percent',
    'Total Vacant',
    'Total Ready Vacant',
    'Percent Rent Ready Homes',

    "Move-Ins",
    "Move-Outs"
]

def process_yardi(df):
    df['installation'] = np.where(
        df.pscode != 'Randolphe Pointe',
        df.pscode.str[:4],
        'brag'
    )
    df['neighborhood'] = np.where(
        df.pscode != 'Randolphe Pointe',
        df.pscode.str[4:],
        df.pscode
    )

    df = df[df.installation != 'rily'].copy()

    df.rename(columns={
        'installation': 'Installation',
        'neighborhood': 'Neighborhood'
    }, inplace=True)

    na_cols_yardi = list(set(col_order) - set(df.columns))
    df[na_cols_yardi] = np.nan

    return df[col_order]

def process_entrata(df):
    df.rename(columns={
        'MarketingName': 'Installation',
        'BuildingName': 'Neighborhood',
        'Units': 'Total Units',
        'Excluded': 'Vacant Offline Homes',
        'Rentable Units': 'Available Units by Week',
        'Vacant Unrented': 'Vacant Unassigned Homes',
        'Notice Unrented': 'Notice Unassigned Homes',
        'Occupied Full': 'Actual Occupied Homes',
        'Vacant Rented': 'Vacant Preleased',
        'Notice Rented': 'Occupied Notice Preleased'
    }, inplace=True)

    df['Availability Percent'] = df['Available Units by Week'] / df['Total Units']
    df['Actual Occupied Homes Percent'] = df['Actual Occupied Homes'] / df['Available Units by Week']
    df['Variance Between Availability and Actual'] = \
        df['Availability Percent'] - df['Actual Occupied Homes Percent']
    df['Leased Percent'] = (
        df['Vacant Preleased'] + df['Actual Occupied Homes'] - df['Notice Unassigned Homes'] 
    ) / df['Available Units by Week']

    na_cols_entrata = list(set(col_order) - set(df.columns))
    df[na_cols_entrata] = np.nan

    return df[col_order]

def process_rp(df):
    df.rename(columns={
        'installation': 'Installation',
        'Subdivision': 'Neighborhood',
        'Total Units by Week': 'Total Units',
        'Availability %': 'Availability Percent',
        'Notices Unassigned Homes': 'Notice Unassigned Homes',
        'Actual Occupied Homes %': 'Actual Occupied Homes Percent',
        'Leased %': 'Leased Percent'
    }, inplace=True)

    na_cols_rp = list(set(col_order) - set(df.columns))
    df[na_cols_rp] = np.nan

    return df[col_order]

def read_or_fail(file, process_fn, df_list):
    try:
        df = pd.read_csv(file)
        df_proc = process_fn(df)
        df_list.append(df_proc)
    except:
        pass

for d in dates:
    yardi_path = f"{output_dir}/{d}-yardi-daily.csv"
    entrata_path = f"{output_dir}/{d}-entrata-daily.csv"
    rp_path = f"{output_dir}/{d}-realpage-daily.csv"
    
    proc_list = []
    read_or_fail(yardi_path, process_yardi, proc_list)
    read_or_fail(entrata_path, process_entrata, proc_list)
    read_or_fail(rp_path, process_rp, proc_list)
    
    df_concat = pd.concat(proc_list)
    print(f"Writing out concat for {d}...")
    df_concat.to_csv(f"{output_dir}/{d}-concat-daily.csv", index=False)

print(f"Writing out Ultra Full Concat")
all_concat_paths = [
    f for f in os.listdir(f"{output_dir}") if re.search("\d{8}-concat-daily.csv", f)
]

concat_dfs = [pd.read_csv(f"{output_dir}/{f}") for f in all_concat_paths]

all_concats_full = pd.concat(concat_dfs) \
    .sort_values(['Installation', 'Neighborhood', 'as_of'])

all_concats_full.to_csv(f"{output_dir}/ultra-full-concat.csv", index=False)






