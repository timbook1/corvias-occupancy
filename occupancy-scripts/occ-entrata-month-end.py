import os
import re
import json
from datetime import datetime
import numpy as np
import pandas as pd

entrata_dir = '../dw-staging/entrata-occupancy'
month_end_dirs = [f"{entrata_dir}/{d}" for d in os.listdir(entrata_dir) if 'json' not in d]
entrata_all_jsons = [f"{d}/{f}" for d in month_end_dirs for f in os.listdir(d)]

def read_entrata(file):
    as_of = re.findall('([a-z]{3})_\d{4}', file)[0]
    with open(file, 'r', encoding='latin-1') as f:
        entrata_str = f.read()

    # Parse JSON properly
    entrata = json.loads(entrata_str.replace('ï»¿', ''))['response']['result']
    
    # Actual property name
    property_name = entrata['Properties']['Property'][0]['MarketingName']
    
    # Table containing relevant occupancy data, cast as data frame
    ils_units = entrata['ILS_Units']['Unit']
    ils_rows = [v['@attributes'] for k, v in ils_units.items()]
    ils = pd.DataFrame(ils_rows)
    ils['MarketingName'] = property_name
    ils['as_of'] = as_of
    
    # Export occupancy data with included base name
    return ils

entrata_data_list = [read_entrata(file) for file in entrata_all_jsons]
ils = pd.concat(entrata_data_list)

# Column #1: Units
ils_main = ils.groupby(['MarketingName', 'BuildingName', 'as_of'])['UnitNumber'].count() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'UnitNumber': 'Units'})

ils_main = ils_main[['MarketingName', 'BuildingName', 'as_of', 'Units']]

ils_tab = ils.groupby(['BuildingName', 'as_of', 'Status'])['UnitNumber'].count() \
    .unstack() \
    .fillna(0) \
    .reset_index()

ils_tab.columns.name = None

# Column #2: Excluded
ils['is_excluded'] = ils['ExcludedReason'].notna() & (ils['ExcludedReason'] != "Corporate Unit")
is_excluded_col = ils.groupby(['BuildingName', 'as_of'])['is_excluded'].sum() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'is_excluded': 'Excluded'})
ils_main = pd.merge(ils_main, is_excluded_col)

# Column #3: Rentable Units
ils_main['Rentable Units'] = ils_main['Units'] - ils_main['Excluded']

# Column #4: Occupied
# According to Reed, Occupied = Occupied + On Notice
ils_occ = ils_tab.copy()
ils_occ['Occupied Full'] = ils_occ[[
    'Occupied (Unavailable)', 'On Notice (Available)', 'On Notice (Unavailable)'
]].sum(axis=1)
ils_occ = ils_occ[['BuildingName', 'as_of', 'Occupied Full']].rename(columns={'Occupied': 'Occupied Full'})
ils_main = pd.merge(ils_main, ils_occ)

# Column #5: Vacant
ils['is_vacant'] = ils['Availability'] == 'Available'
ils_vacant = ils.groupby(['BuildingName', 'as_of'])['is_vacant'].sum() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'is_vacant': 'Vacant'})
ils_main = pd.merge(ils_main, ils_vacant)

# Column #6: Available
ils_main['Available'] = ils_main['Units'] - ils_main['Vacant']

# Column #7: Occupied No Notice
# TODO
ils_main['Occupied No Notice'] = np.nan

# Column #8: Notice Rented
# Column #9: Notice Unrented
ils_notice_rented = ils_tab[
        ['BuildingName', 'as_of', 'On Notice (Unavailable)', 'On Notice (Available)']
    ] \
    .rename(columns={
        'On Notice (Unavailable)': 'Notice Rented',
        'On Notice (Available)': 'Notice Unrented',
    }) \
    .copy()

ils_main = pd.merge(ils_main, ils_notice_rented)

# Column #10: Vacant Rented
ils_tab['Vacant Rented'] = ils_tab['Vacant Not Ready (Unavailable)'] + ils_tab['Vacant Ready (Unavailable)']

# Column #11: Vacant Unrented
ils_tab['Vacant Unrented'] = ils_tab['Vacant Ready (Available)'] + ils_tab['Vacant Not Ready (Available)']

ils_main = pd.merge(
    ils_main,
    ils_tab[['BuildingName', 'as_of', 'Vacant Rented', 'Vacant Unrented']]
)

month_end_map = {
    'jan': '1/31/21',
    'feb': '2/28/21',
    'mar': '3/31/21',
    'apr': '4/30/21',
    'may': '5/31/21',
    'jun': '6/30/21',
    'jul': '7/30/21',
    'aug': '8/31/21',
    'sep': '9/30/21',
    'oct': '10/31/21',
    'nov': '11/30/21',
    'dec': '12/31/21'
}

ils_main['as_of'] = pd.to_datetime(ils_main['as_of'].map(month_end_map))
ils_main.sort_values(['MarketingName', 'BuildingName', 'as_of'], inplace=True)

today = datetime.now().strftime('%Y%m%d')
ils_main.to_csv(f'output/{today}-monthly-entrata-occupancy.csv', index=False)
