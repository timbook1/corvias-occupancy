import os
import re
import json
from datetime import datetime
import numpy as np
import pandas as pd

entrata_dir = '../dw-staging/entrata-occupancy'
occ_files = [
    f
    for f in os.listdir(entrata_dir)
    if re.search('entrataOccupancy\d{6}_\d{4}-\d{2}-\d{2}.json', f)
]

def read_entrata(file):
    with open(os.path.join(entrata_dir, file), 'r', encoding='latin-1') as f:
        entrata_str = f.read()

    # Parse JSON properly
    entrata = json.loads(entrata_str.replace('ï»¿', ''))['response']['result']
    
    # Actual property name
    property_name = entrata['Properties']['Property'][0]['MarketingName']
    
    # Unnecessary for now?
    # floorplans = entrata['Properties']['Property'][0]['Floorplans']['Floorplan']
    
    # Table containing relevant occupancy data, cast as data frame
    ils_units = entrata['ILS_Units']['Unit']
    ils_rows = [v['@attributes'] for k, v in ils_units.items()]
    ils = pd.DataFrame(ils_rows)
    ils['MarketingName'] = property_name
    
    # Pull date
    dt = re.findall('\d{4}-\d{2}-\d{2}', file)[0]
    ils['as_of'] = dt
    
    # Export occupancy data with included base name
    return ils

entrata_data_list = [read_entrata(file) for file in occ_files]
ils = pd.concat(entrata_data_list)

# Column #1: Units
ils_main = ils \
    .groupby(['MarketingName', 'BuildingName', 'as_of'])['UnitNumber'].count() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'UnitNumber': 'Units'})

ils_main = ils_main[['MarketingName', 'BuildingName', 'as_of', 'Units']]

ils_tab = ils \
    .groupby(['BuildingName', 'as_of', 'Status'])['UnitNumber'].count() \
    .to_frame() \
    .reset_index() \
    .pivot_table(
        index=['BuildingName', 'as_of'],
        columns='Status',
        values='UnitNumber',
        fill_value=0
    ).reset_index()

# Column #2: Excluded
ils['is_excluded'] = ils['ExcludedReason'].notna() & (ils['ExcludedReason'] != "Corporate Unit")
is_excluded_col = ils \
    .groupby(['BuildingName', 'as_of'])['is_excluded'].sum() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'is_excluded': 'Excluded'})
ils_main = pd.merge(ils_main, is_excluded_col)

# Column #3: Rentable Units
ils_main['Rentable Units'] = ils_main['Units'] - ils_main['Excluded']

# Column #4: Occupied
# According to Reed, Occupied = Occupied + On Notice
ils_occ = ils_tab.copy()
ils_occ['Occupied Full'] = ils_occ[[
    'Occupied (Unavailable)', 'On Notice (Available)', 'On Notice (Unavailable)'
]].sum(axis=1)
ils_occ = ils_occ[['BuildingName', 'as_of', 'Occupied Full']].rename(columns={'Occupied': 'Occupied Full'})
ils_main = pd.merge(ils_main, ils_occ)

# Column #5: Vacant
ils['is_vacant'] = ils['Availability'] == 'Available'
ils_vacant = ils.groupby(['BuildingName', 'as_of'])['is_vacant'].sum() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'is_vacant': 'Vacant'})
ils_main = pd.merge(ils_main, ils_vacant)

# Column #6: Available
ils_main['Available'] = ils_main['Units'] - ils_main['Vacant']

# Column #7: Occupied No Notice
# TODO
ils_main['Occupied No Notice'] = np.nan

# Column #8: Notice Rented
# Column #9: Notice Unrented
ils_notice_rented = ils_tab[['BuildingName', 'as_of', 'On Notice (Unavailable)', 'On Notice (Available)']] \
    .rename(columns={
        'On Notice (Unavailable)': 'Notice Rented',
        'On Notice (Available)': 'Notice Unrented',
    }) \
    .copy()
ils_main = pd.merge(ils_main, ils_notice_rented)

# Column #10: Vacant Rented
ils_tab['Vacant Rented'] = ils_tab['Vacant Not Ready (Unavailable)'] + ils_tab['Vacant Ready (Unavailable)']

# Column #11: Vacant Unrented
ils_tab['Vacant Unrented'] = ils_tab['Vacant Ready (Available)'] + ils_tab['Vacant Not Ready (Available)']

# New Column: Total Vacant
ils_tab['Total Vacant'] = \
    ils_tab['Vacant Not Ready (Available)'] + \
    ils_tab['Vacant Not Ready (Unavailable)'] + \
    ils_tab['Vacant Ready (Available)'] + \
    ils_tab['Vacant Ready (Unavailable)']

# New Column: Total Ready Vacant
ils_tab['Total Ready Vacant'] = ils_tab['Vacant Ready (Available)'] + ils_tab['Vacant Ready (Unavailable)']

ils_main = pd.merge(
    ils_main,
    ils_tab[['BuildingName', 'as_of', 'Vacant Rented', 'Vacant Unrented', 'Total Vacant', 'Total Ready Vacant']]
)

# New Derived Column: Net Total Homes Available
ils['is_net_total_avail'] = ils.Status.isin([
    'Vacant Ready (Available)', 'Vacant Not Ready (Available)', 'On Notice (Available)'
]) & ~(
    ils.FloorPlanName.str.lower().str.contains('offline') | \
    ils.ExcludedReason.str.lower().str.contains('corporate unit')
)

is_net_total_avail = ils \
    .groupby(['BuildingName', 'as_of'])['is_net_total_avail'].sum() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'is_net_total_avail': 'Net Total Homes Available'})
ils_main = pd.merge(ils_main, is_net_total_avail)

# New Derived Column: Total Availability
ils_main['Total Availability'] = ils_main['Net Total Homes Available'] / ils_main['Rentable Units']

# New Derived Column: Percent Rent Ready Homes
ils_main['Percent Rent Ready Homes'] = ils_main['Vacant Unrented'] / ils_main['Occupied Full']

def calc_move_ins_outs(df):
    df['occ_bit_today'] = (df.Status == 'Occupied (Unavailable)')*1
    df['occ_bit_yesterday'] = df['occ_bit_today'].shift()
    df['occ_bit_diff'] = df['occ_bit_today'] - df['occ_bit_yesterday']
    df['move_in_out'] = df['occ_bit_diff'].map({
        np.nan: 'no change',
        0: 'no change',
        1: 'move-in',
        -1: 'move-out'
    })
    return df[['BuildingName', 'as_of', 'UnitNumber', 'move_in_out']]

entrata_moves = ils.groupby('UnitNumber').apply(calc_move_ins_outs)
entrata_moves['is_move_in'] = (entrata_moves['move_in_out'] == 'move-in')*1
entrata_moves['is_move_out'] = (entrata_moves['move_in_out'] == 'move-out')*1

entrata_move_grps = entrata_moves \
    .groupby(['BuildingName', 'as_of'], as_index=False)[['is_move_in', 'is_move_out']].sum() \
    .rename(columns={'is_move_in': 'Move-Ins', 'is_move_out': 'Move-Outs'})
ils_main = pd.merge(ils_main, entrata_move_grps)

dates = ils_main.as_of.value_counts().sort_index().index.values
for date in dates:
    print(f"Processing Entrata date: {date}")
    ils_main[ils_main.as_of == date].to_csv(f'output/{date.replace("-", "")}-entrata-daily.csv', index=False)
