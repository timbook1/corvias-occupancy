from datetime import datetime
import subprocess

now = datetime.now().strftime("%b %d, %Y")

print(now + '='*60)
print("Processing Yardi")
subprocess.run(['python3', 'occ-yardi-process.py'])

print("Processing Entrata")
subprocess.run(['python3', 'occ-entrata-process.py'])

print("Processing RealPage")
subprocess.run(['python3', 'occ-realpage-process.py'])

print("Concatenating into full data...")
subprocess.run(['python3', 'data-concat.py'])
