import os
import re
from functools import reduce
import numpy as np
import pandas as pd
from datetime import datetime

rp_dir = '../input/realpage-monthly/'
installations = os.listdir(rp_dir)

def read_file(file):
    base, as_of = re.findall("input/realpage-monthly/(.*)/([a-z]{3})\d{2,4}.csv", file)[0]
    df = pd.read_csv(file)
    df['installation'] = base
    df['as_of'] = as_of
    return df

installation_dirs = [os.path.join(rp_dir, i) for i in os.listdir(rp_dir)]

all_files = []
for d in installation_dirs:
    for f in os.listdir(d):
        all_files += [f"{d}/{f}"]

dfs = [read_file(f) for f in all_files]
rp = pd.concat(dfs, axis=0)

# Field 1: Total Units by Week
f1 = rp \
    .groupby(['installation', 'Subdivision', 'as_of'], as_index=False)['Unit'] \
    .count() \
    .rename(columns={'Unit': 'Total Units by Week'})

def count_by_group(df, name, *args):
    s_count = df \
        .groupby(['installation', 'Subdivision', 'as_of'], as_index=False) \
        .apply(lambda grp: grp['Unit Status'].isin(args).sum())
    s_count.columns = ['installation', 'Subdivision', 'as_of', name]
    return s_count

fields = []

# Field #2: Vacant Offline Homes
f2 = count_by_group(rp, 'Vacant Offline Homes', 'Admin/Down')
fields.append(f2)

# Field #3: Available Units by Week
# F1 - F2
f3 = pd.merge(f1, f2)
f3['Available Units by Week'] = f3['Total Units by Week'] - f3['Vacant Offline Homes']
f3.drop(columns=['Total Units by Week', 'Vacant Offline Homes'], inplace=True)
fields.append(f3)

# Field #4: Availability %
# F3 / F1
f4 = pd.merge(f1, f3)
f4['Availability %'] = f4['Available Units by Week'] / f4['Total Units by Week']
f4.drop(columns=['Available Units by Week', 'Total Units by Week'], inplace=True)
fields.append(f4)

# Field #5: Vacant Unassigned Homes
f5 = count_by_group(rp, 'Vacant Unassigned Homes', 'Vacant')
fields.append(f5)

# Field #6: Notices Unassigned Homes
f6 = count_by_group(rp, 'Notices Unassigned Homes', 'Occupied-NTV')
fields.append(f6)

# Field #7: Net Total Homes Available
# F5 + F6
f7 = pd.merge(f5, f6)
f7['Net Total Homes Available'] = f7['Vacant Unassigned Homes'] + f7['Notices Unassigned Homes']
f7.drop(columns=['Vacant Unassigned Homes', 'Notices Unassigned Homes'], inplace=True)
fields.append(f7)

# Field #8: Total Availability
# F7 / F3
f8 = pd.merge(f3, f7)
f8['Total Availability'] = f8['Net Total Homes Available'] / f8['Available Units by Week']
f8.drop(columns=['Net Total Homes Available', 'Available Units by Week'], inplace=True)
fields.append(f8)

# Field #9: Budgeted Monthly Occupancy %
# TODO: Where does this come from?
# Listed as "Coming from Host"

# Field #11: Variance between Budget and Actual
# F10 - F9
# TODO: Can't do this one without Field #9!

# Field #16: Actual Occupied Homes
f16 = count_by_group(
    rp,
    'Actual Occupied Homes',
    'Occupied', 'Occupied-NTVL', 'Occupied-NTV'
)
fields.append(f16)

# Field #10: Actual Occupied Homes %
# F16 / F3
f10 = pd.merge(f3, f16)
f10['Actual Occupied Homes %'] = f10['Actual Occupied Homes'] / f10['Available Units by Week']
f10.drop(columns=['Actual Occupied Homes', 'Available Units by Week'], inplace=True)
fields.append(f10)

# Field #12: Variance between Availability and Actual
# F4 - F10
f12 = pd.merge(f4, f10)
f12['Variance between Availability and Actual'] = f12['Availability %'] - f12['Actual Occupied Homes %']
f12.drop(columns=['Availability %', 'Actual Occupied Homes %'], inplace=True)
fields.append(f12)

# Field #17: Vacant Preleased
f17 = count_by_group(rp, 'Vacant Preleased', 'Vacant-Leased')
fields.append(f17)

# Field #18: Occupied Notice Preleased
f18 = count_by_group(rp, 'Occupied Notice Preleased', 'Occupied-NTVL')
fields.append(f18)

# Field #19: Leased %
# (F17 + F16 - F6) / F3
f19 = reduce(pd.merge, [f3, f6, f16, f17])
f19['Leased %'] = (
        f19['Vacant Preleased'] + \
        f19['Actual Occupied Homes'] - \
        f19['Notices Unassigned Homes']
    ) / f19['Available Units by Week']

f19.drop(columns=[
    'Vacant Preleased', 'Actual Occupied Homes', 'Notices Unassigned Homes', 'Available Units by Week'
], inplace=True)
fields.append(f19)

# Field 28: Total Vacant
# F3 - F16
f28 = pd.merge(f3, f16)
f28['Total Vacant'] = f28['Available Units by Week'] - f28['Actual Occupied Homes']
f28.drop(columns=['Available Units by Week', 'Actual Occupied Homes'], inplace=True)
fields.append(f28)

rp_occ = reduce(pd.merge, [f1, *fields])

month_end_map = {
    'jan': '1/31/21',
    'feb': '2/28/21',
    'mar': '3/31/21',
    'apr': '4/30/21',
    'may': '5/31/21',
    'jun': '6/30/21',
    'jul': '7/30/21',
    'aug': '8/31/21',
    'sep': '9/30/21',
    'oct': '10/31/21',
    'nov': '11/30/21',
    'dec': '12/31/21'
}

rp_occ['as_of'] = pd.to_datetime(rp_occ['as_of'].map(month_end_map))

rp_occ.sort_values(['installation', 'Subdivision', 'as_of'], inplace=True)

today = datetime.now().strftime('%Y%m%d')
rp_occ.to_csv(f'output/{today}-monthly-realpage-occupancy.csv', index=False)
