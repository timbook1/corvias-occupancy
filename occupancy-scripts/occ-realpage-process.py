import os
import re
import numpy as np
import pandas as pd
from functools import reduce
from datetime import datetime

as_of = datetime.now().strftime('%Y-%m-%d')

files = [
    f for f in os.listdir('../dw-staging/realpage-occupancy/')
    if re.search('military_housing_rent_roll_detail', f)
]

def read_by_file(file):
    as_of = datetime.strptime(re.findall('^\d{8}', file)[0], '%Y%m%d').strftime('%Y-%m-%d')
    installation = re.findall('^\d{8}_(.*)_military_housing', file)[0].title()
    df = pd.read_csv(os.path.join('../dw-staging/realpage-occupancy', file))
    df['as_of'] = as_of
    df['installation'] = installation
    return df

data_list = [read_by_file(f) for f in files]
rp = pd.concat(data_list)

# Field 1: Total Units by Week
rpm = rp \
    .groupby(['installation', 'Subdivision', 'as_of'], as_index=False)['Unit'] \
    .count() \
    .rename(columns={'Unit': 'Total Units by Week'})

def count_by_group(df, name, *args):
    df[name] = np.where(df['Unit Status'].isin(args), 1, 0)
    df_grp = df.groupby(['installation', 'Subdivision', 'as_of'], as_index=False)[name].sum()
    return df_grp[['installation', 'Subdivision', 'as_of', name]]

# Field #2: Vacant Offline Homes
f2 = count_by_group(rp, 'Vacant Offline Homes', 'Admin/Down')
rpm = rpm.merge(f2)

# Field #3: Available Units by Week
# F1 - F2
rpm['Available Units by Week'] = rpm['Total Units by Week'] - rpm['Vacant Offline Homes']

# Field #4: Availability %
# F3 / F1
rpm['Availability %'] = rpm['Available Units by Week'] / rpm['Total Units by Week']

# Field #5: Vacant Unassigned Homes
f5 = count_by_group(rp, 'Vacant Unassigned Homes', 'Vacant')
rpm = rpm.merge(f5)

# Field #6: Notices Unassigned Homes
f6 = count_by_group(rp, 'Notices Unassigned Homes', 'Occupied-NTV')
rpm = rpm.merge(f6)

# Field #7: Net Total Homes Available
# F5 + F6
rpm['Net Total Homes Available'] = rpm['Vacant Unassigned Homes'] + rpm['Notices Unassigned Homes']

# Field #8: Total Availability
# F7 / F3
rpm['Total Availability'] = rpm['Net Total Homes Available'] / rpm['Available Units by Week']

# Field #9: Budgeted Monthly Occupancy %
# TODO: Where does this come from?
# Listed as "Coming from Host"

# Field #11: Variance between Budget and Actual
# F10 - F9
# TODO: Can't do this one without Field #9!

# Field #16: Actual Occupied Homes
f16 = count_by_group(
    rp,
    'Actual Occupied Homes',
    'Occupied', 'Occupied-NTVL', 'Occupied-NTV'
)
rpm = rpm.merge(f16)

# Field #10: Actual Occupied Homes %
# F16 / F3
rpm['Actual Occupied Homes %'] = rpm['Actual Occupied Homes'] / rpm['Available Units by Week']

# Field #12: Variance between Availability and Actual
# F4 - F10
rpm['Variance between Availability and Actual'] = rpm['Availability %'] - rpm['Actual Occupied Homes %']

# Field #17: Vacant Preleased
f17 = count_by_group(rp, 'Vacant Preleased', 'Vacant-Leased')
rpm = rpm.merge(f17)

# Field #18: Occupied Notice Preleased
f18 = count_by_group(rp, 'Occupied Notice Preleased', 'Occupied-NTVL')
rpm = rpm.merge(f18)

# Field #19: Leased %
# (F17 + F16 - F6) / F3
rpm['Leased %'] = (
        rpm['Vacant Preleased'] + \
        rpm['Actual Occupied Homes'] - \
        rpm['Notices Unassigned Homes']
    ) / rpm['Available Units by Week']

# Field 28: Total Vacant
# F3 - F16
rpm['Total Vacant'] = rpm['Available Units by Week'] - rpm['Actual Occupied Homes']

today = datetime.now().strftime('%Y%m%d')

dates = rpm.as_of.unique()
for d in dates:
    print(f"Writing RealPage date {d}...")
    rpm_date = rpm[rpm.as_of == d]
    d_strip = d.replace('-', '')
    rpm_date.to_csv(f'output/{d_strip}-realpage-daily.csv', index=False)
