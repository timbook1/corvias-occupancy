from datetime import datetime
import os
import re
import numpy as np
import pandas as pd

DATA_DIR = "../dw-staging/yardi-occupancy"

month_end_files = [
    (re.findall('[a-z]{3}', f)[0], f)
    for f in os.listdir(DATA_DIR)
    if re.search('[a-z]{3}_\d{4}.csv', f)
]

def read_and_add_date(file, month):
    df = pd.read_csv(file)
    df['as_of'] = month
    return df

yardi_month_end = [
    read_and_add_date(f"{DATA_DIR}/{f}", m)
    for m, f in month_end_files
]

yardi = pd.concat(yardi_month_end)

# Field #1: Total Units
yardi_main = yardi.groupby(['pscode', 'as_of'])['Type'].count().to_frame().reset_index()
yardi_main.rename(columns={'Type': 'Total Units'}, inplace=True)

# Field #2: Vacant Offline Homes
# TODO: Exclude "Offline - Combined"? Unsure
yardi['is_offline'] = yardi['opstatus'].str.lower().str.contains('offline')
is_offline = yardi.groupby(['pscode', 'as_of'])['is_offline'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_offline, how='left').rename(columns={'is_offline': 'Vacant Offline Homes'})

# Field #3: Available Units by Week
yardi_main['Available Units by Week'] = yardi_main['Total Units'] - yardi_main['Vacant Offline Homes']

# Field #4: Availability Percent
yardi_main['Availability Percent'] = yardi_main['Available Units by Week'] / yardi_main['Total Units']

# Field #5: Vacant Unassigned Homes
yardi['is_vac_unassign'] = yardi['EventType'].isin([
    'Vacant Unrented Ready', 'Vacant Unrented Not Ready'
])
is_vac_unassign = yardi.groupby(['pscode', 'as_of'])['is_vac_unassign'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_vac_unassign, how='left').rename(columns={'is_vac_unassign': 'Vacant Unassigned Homes'})
yardi_main['Vacant Unassigned Homes'] = yardi_main['Vacant Unassigned Homes'] - yardi_main['Vacant Offline Homes']

# Field #6: Notice Unassigned Homes
yardi['is_notice_unassign'] = yardi['EventType'].isin(['Notice Unrented'])
is_notice_unassign = yardi.groupby(['pscode', 'as_of'])['is_notice_unassign'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_notice_unassign, how='left').rename(columns={'is_notice_unassign': 'Notice Unassigned Homes'})

# Field #7: Net Total Homes Available (#5 + #6)
yardi_main['Net Total Homes Available'] = yardi_main['Vacant Unassigned Homes'] + yardi_main['Notice Unassigned Homes']

# Field #8: Total Availability (#7 / #3)
yardi_main['Total Availability'] = yardi_main['Net Total Homes Available'] / yardi_main['Available Units by Week']

# Field #9: Budgeted Monthly Occupancy %
# TODO: ???

# Field #11: Variance between Budget and Actual
# row 10 - row 9

# Field #16: Actual Occupied Homes
yardi['is_act_occ'] = yardi['EventType'].isin([
    'Occupied No Notice', 'Notice Rented', 'Notice Unrented'
])
is_act_occ = yardi.groupby(['pscode', 'as_of'])['is_act_occ'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_act_occ, how='left').rename(columns={'is_act_occ': 'Actual Occupied Homes'})

# Field #10: Actual Occupied Homes %
# actual occupied (16) divided by #3
yardi_main['Actual Occupied Homes Percent'] = yardi_main['Actual Occupied Homes'] / yardi_main['Available Units by Week']

# Field #12: Variance between Availability and Actual
# row 4 - row 10
yardi_main['Variance Between Availability and Actual'] = yardi_main['Availability Percent'] - yardi_main['Actual Occupied Homes Percent']

# Field #17: Vacant Preleased
yardi['is_vac_pre'] = yardi['EventType'].isin(['Vacant Rented Ready', 'Vacant Rented Not Ready'])
is_vac_pre = yardi.groupby(['pscode', 'as_of'])['is_vac_pre'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_vac_pre, how='left').rename(columns={'is_vac_pre': 'Vacant Preleased'})

# Field #18: Occupied Notice Preleased
yardi['is_occ_notice'] = yardi['EventType'].isin(['Notice Rented'])
is_occ_notice = yardi.groupby(['pscode', 'as_of'])['is_occ_notice'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_occ_notice, how='left').rename(columns={'is_occ_notice': 'Occupied Notice Preleased'})

# Field #19: Leased Percent
yardi_main['Leased Percent'] = (
    yardi_main['Vacant Preleased'] + \
    yardi_main['Actual Occupied Homes'] - \
    yardi_main['Notice Unassigned Homes']
) / yardi_main['Available Units by Week']

# Field #28: Total Vacant (Excludes Preleases & Down Units) (#3 - #16)
yardi_main['Total Vacant'] = yardi_main['Available Units by Week'] - yardi_main['Actual Occupied Homes']

# Field #29: Total Ready Vacant (#5?)
# TODO: Is this right?
# #5 - vacant preleased (but only the ready ones)
# "Vacant but unrented" = Vacant Unrented Ready
yardi_main['Total Ready Vacant'] = yardi_main['Vacant Unassigned Homes'] - yardi_main['Vacant Preleased']

# Field #30: Percent Rent Ready Homes (Excludes Pre-leases) (#29 / #28)
yardi_main['Percent Rent Ready Homes'] = yardi_main['Vacant Unassigned Homes'] / yardi_main['Actual Occupied Homes']

month_end_map = {
    'jan': '1/31/21',
    'feb': '2/28/21',
    'mar': '3/31/21',
    'apr': '4/30/21',
    'may': '5/31/21',
    'jun': '6/30/21',
    'jul': '7/30/21',
    'aug': '8/31/21',
    'sep': '9/30/21',
    'oct': '10/31/21',
    'nov': '11/30/21',
    'dec': '12/31/21'
}

yardi_main['as_of'] = pd.to_datetime(yardi_main['as_of'].map(month_end_map))

yardi_main['installation'] = yardi_main.pscode.str[:4]
yardi_main['neighborhood'] = yardi_main.pscode

yardi_cols = yardi_main.columns.tolist()
output_cols = ['installation', 'neighborhood'] + yardi_cols[1:-2]

yardi_main = yardi_main[output_cols]
yardi_main.sort_values(['installation', 'neighborhood', 'as_of'], inplace=True)

today = datetime.now().strftime('%Y%m%d')
yardi_main.to_csv(f'output/{today}-monthly-yardi-occupancy.csv', index=False)
