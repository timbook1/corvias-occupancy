from datetime import datetime
import os
import re
import numpy as np
import pandas as pd
pd.set_option('display.max_columns', 999)

def read_and_stamp(file):
    dt = re.findall('(\d{4}-\d{2}-\d{2})', file)[0]
    df = pd.read_csv(f"../dw-staging/yardi-occupancy/daily/{file}")
    df['as_of'] = dt
    return df

yardi_occ_dfs = [
    read_and_stamp(file)
    for file in os.listdir('../dw-staging/yardi-occupancy/daily')
    if re.search('yardiOccupancy_\d{4}-\d{2}-\d{2}.csv', file)
]

yardi = pd.concat(yardi_occ_dfs).drop_duplicates()

# Field #1: Total Units
yardi_main = yardi \
    .groupby(['pscode', 'as_of'])['Type'].count() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'Type': 'Total Units'})

# Field #2: Vacant Offline Homes
# TODO: Exclude "Offline - Combined"? Unsure
yardi['is_offline'] = yardi['opstatus'].str.lower().str.contains('offline')
is_offline = yardi.groupby(['pscode', 'as_of'])['is_offline'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_offline, how='left') \
    .rename(columns={'is_offline': 'Vacant Offline Homes'})

# Field #3: Available Units by Week
yardi_main['Available Units by Week'] = yardi_main['Total Units'] - yardi_main['Vacant Offline Homes']

# Field #4: Availability Percent
yardi_main['Availability Percent'] = yardi_main['Available Units by Week'] / yardi_main['Total Units']

# Field #5: Vacant Unassigned Homes
yardi['is_vac_unassign'] = yardi['EventType'].isin([
    'Vacant Unrented Ready', 'Vacant Unrented Not Ready'
])
is_vac_unassign = yardi.groupby(['pscode', 'as_of'])['is_vac_unassign'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_vac_unassign, how='left') \
    .rename(columns={'is_vac_unassign': 'Vacant Unassigned Homes'})
yardi_main['Vacant Unassigned Homes'] = yardi_main['Vacant Unassigned Homes'] - yardi_main['Vacant Offline Homes']

# Field #6: Notice Unassigned Homes
yardi['is_notice_unassign'] = yardi['EventType'].isin(['Notice Unrented'])
is_notice_unassign = yardi.groupby(['pscode', 'as_of'])['is_notice_unassign'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_notice_unassign, how='left') \
    .rename(columns={'is_notice_unassign': 'Notice Unassigned Homes'})

# Field #7: Net Total Homes Available (#5 + #6)
yardi_main['Net Total Homes Available'] = yardi_main['Vacant Unassigned Homes'] + yardi_main['Notice Unassigned Homes']

# Field #8: Total Availability (#7 / #3)
yardi_main['Total Availability'] = yardi_main['Net Total Homes Available'] / yardi_main['Available Units by Week']

# Field #9: Budgeted Monthly Occupancy %
# TODO: ???

# Field #11: Variance between Budget and Actual
# row 10 - row 9

# Field #16: Actual Occupied Homes
yardi['is_act_occ'] = yardi['EventType'].isin([
    'Occupied No Notice', 'Notice Rented', 'Notice Unrented'
])
is_act_occ = yardi.groupby(['pscode', 'as_of'])['is_act_occ'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_act_occ, how='left') \
    .rename(columns={'is_act_occ': 'Actual Occupied Homes'})

# Field #10: Actual Occupied Homes %
# actual occupied (16) divided by #3
yardi_main['Actual Occupied Homes Percent'] = yardi_main['Actual Occupied Homes'] / yardi_main['Available Units by Week']

# Field #12: Variance between Availability and Actual
# row 4 - row 10
yardi_main['Variance Between Availability and Actual'] = yardi_main['Availability Percent'] - yardi_main['Actual Occupied Homes Percent']

# Field #17: Vacant Preleased
yardi['is_vac_pre'] = yardi['EventType'].isin(['Vacant Rented Ready', 'Vacant Rented Not Ready'])
is_vac_pre = yardi.groupby(['pscode', 'as_of'])['is_vac_pre'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_vac_pre, how='left')     .rename(columns={'is_vac_pre': 'Vacant Preleased'})

# Field #18: Occupied Notice Preleased
yardi['is_occ_notice'] = yardi['EventType'].isin(['Notice Rented'])
is_occ_notice = yardi.groupby(['pscode', 'as_of'])['is_occ_notice'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_occ_notice, how='left').rename(columns={'is_occ_notice': 'Occupied Notice Preleased'})

# Field #19: Leased Percent
yardi_main['Leased Percent'] = (
    yardi_main['Vacant Preleased'] + \
    yardi_main['Actual Occupied Homes'] - \
    yardi_main['Notice Unassigned Homes']
) / yardi_main['Available Units by Week']

# Field #28: Total Vacant (Excludes Preleases & Down Units) (#3 - #16)
yardi_main['Total Vacant'] = yardi_main['Available Units by Week'] - yardi_main['Actual Occupied Homes']

# Field #29: Total Ready Vacant 
# From Reed 8/9: Vac Rented Ready + Vac Unrented Ready
yardi['is_total_ready_vac'] = yardi['EventType'].isin(['Vacant Rented Ready', 'Vacant Unrented Ready'])
is_vac_pre = yardi.groupby(['pscode', 'as_of'])['is_total_ready_vac'].sum().reset_index()
yardi_main = pd.merge(yardi_main, is_vac_pre, how='left') \
    .rename(columns={'is_total_ready_vac': 'Total Ready Vacant'})

# Field #30: Percent Rent Ready Homes (Excludes Pre-leases) (#29 / #28)
yardi_main['Percent Rent Ready Homes'] = yardi_main['Vacant Unassigned Homes'] / yardi_main['Actual Occupied Homes']

# Calculate Move-Ins/Outs
def calc_move_ins_outs(df):
    df = df.sort_values('as_of')
    df['occ_bit_today'] = np.where(df['EventType'] == 'Occupied No Notice', 1, 0)
    df['occ_bit_yesterday'] = df['occ_bit_today'].shift()
    df['occ_bit_diff'] = df['occ_bit_today'] - df['occ_bit_yesterday']
    df['move_in_out'] = df['occ_bit_diff'].map({
        np.nan: 'no change',
        0: 'no change',
        1: 'move-in',
        -1: 'move-out'
    })
    return df[['pscode', 'unit_hmy', 'as_of', 'move_in_out']]
    
yardi_moves = yardi \
    .groupby(['pscode', 'unit_hmy'], as_index=False) \
    .apply(calc_move_ins_outs) \
    .reset_index(drop=True)

yardi_moves['is_move_in'] = (yardi_moves['move_in_out'] == 'move-in')*1
yardi_moves['is_move_out'] = (yardi_moves['move_in_out'] == 'move-out')*1

yardi_move_grps = yardi_moves \
    .groupby(['pscode', 'as_of'], as_index=False)[['is_move_in', 'is_move_out']].sum() \
    .rename(columns={'is_move_in': 'Move-Ins', 'is_move_out': 'Move-Outs'})

yardi_main = yardi_main.merge(yardi_move_grps, on=['pscode', 'as_of'])

# From Reed: We want to reassign this one totally away from Bragg
yardi_main['pscode'] = np.where(
    yardi_main['pscode'].str.strip() == 'bragrpt',
    'Randolphe Pointe',
    yardi_main['pscode'].str.strip()
)

dates = yardi_main.as_of.value_counts().sort_index().index.values

for date in dates:
    print(f"Processing Yardi date: {date}")
    yardi_main[yardi_main.as_of == date].to_csv(f'output/{date.replace("-", "")}-yardi-daily.csv', index=False)
